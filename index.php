<!-- The first include should be config.php -->
<?php require_once('config.php') ?>
<?php require_once( ROOT_PATH . '/includes/public_functions.php') ?>

<!--Ritorna i post dal database---->
<?php $posts = getPublishedPosts(); ?>

<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>
<title>LifeBlog | Home </title>
</head>

<body>
	<!-- container - wraps whole page -->

	<!-- navbar -->
	<?php include(ROOT_PATH . '/includes/navbar.php') ?>
	<!-- // navbar -->



	<!-- Page content -->
	<section class="mainsection">
		<div class="container">
			<div class="row" style="margin-top:4rem ">
				<div class="col-lg-5 col-sm-12">
					<div class="cont" style="margin-bottom:30px ">
						<h1>My Community Blog</h1>
					</div>
					<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iure quaerat ullam quae ipsum iusto eligendi ducimus voluptates praesentium delectus aspernatur sequi earum nesciunt fuga sapiente voluptatem vel, ab blanditiis. Officiis, exercitationem ut rem asperiores harum quasi doloremque earum possimus? Eius repudiandae veritatis reiciendis commodi praesentium numquam aliquam mollitia officiis corporis!</p>

					<a href="#" class="btn btn-lg btn-success mybtn">Read More</a>
				</div>
			</div>
		</div>
	</section>
	

	<section class=article>
		<div class="container">
			<div class="row">
				<?php foreach ($posts as $post): ?>
				<div class="col-12 col-sm-8 col-md-6 col-lg-4 mt-4 d-flex">
					<div class="card" >
					<img src="<?php echo BASE_URL . '/static/images/' . $post['image']; ?>" class="card-image" alt="" style="max-height: 300px">

						<div class="card-body">
							<h4 class="card-title"><?php echo $post['title'] ?></h4>
							<small class="text-muted cat">
								<i class="far fa-clock text-info"></i> 30 minutes
								<i class="fas fa-users text-info"></i> 4 portions
							</small>
							<p class="card-text"><?php echo $post['description'] ?></p>
							<a href="single_post.php?post-slug=<?php echo $post['slug'] ?>" class="btn btn-outline-success">Read article</a>
						</div>
						<div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
							<div class="views"><?php echo $post['created_at']?>
							</div>
							<div class="stats">
								<i class="far fa-eye"></i> <?php echo $post['views']?>
	
							</div>

						</div>
					</div>
				</div>
				<?php endforeach?>
			</div>

				

	</section>
	<!-- // Page content -->

	<!-- footer -->
	<!-- // footer -->
	<?php include(ROOT_PATH . '/includes/footer.php') ?>