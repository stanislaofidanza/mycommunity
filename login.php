<?php require_once('config.php') ?>
<?php require_once(ROOT_PATH . '/includes/public_functions.php') ?>
<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>
<?php require_once(ROOT_PATH . '/includes/registration_login.php') ?>
<?php include(ROOT_PATH . '/includes/navbar.php') ?>
<?php if ($_SESSION['loggedIn'] === 'yes') {
  header('location: ' . BASE_URL . 'admin/dashboard.php');
}
?>


<div class="container">
  <div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto" style="margin-top: 30px">
      <div class="card card-signin my-5">
        <div class="card-body">
          <h5 class="card-title text-center">Sign In</h5>
          <form class="form-signin" method="post" action="?php echo BASE_URL . 'index.php'">
          <?php include(ROOT_PATH . '/includes/errors.php') ?>
            <div class="form-label-group">
           

              
                <input type="text" name="username" id="us" class="form-control">
                <label for="us">Username</label>
             
            

            </div>

            <div class="form-label-group">
              <input type="password" name="password" class="form-control" >
              <label for="us">Password</label>
            </div>

            <div class="custom-control custom-checkbox mb-3">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">Remember password</label>
            </div>
            <button class="btn btn-lg btn-primary btn-block text-uppercase signbtn" type="submit" name="login_btn" style="border: #36be58">Sign in</button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>