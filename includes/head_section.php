<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Blog</title>

	<!-----main stylesheet----->
	<link rel="stylesheet" href="/static/css/style.css">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Averia+Serif+Libre|Noto+Serif|Tangerine" rel="stylesheet">

	<meta charset="UTF-8">
	<!--------Bootstrap CSS-------->
	<link rel="stylesheet" href="/static/css/bootstrap.min.css">

	<!-----Bootstrap Js------------>
	<script src="/static/js/bootstrap.min.js" defer></script>
	<!-----Font------->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

	<!-----iCON---->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

	<!-----Editor---->
	<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>


	<script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>