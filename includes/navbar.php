<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">

  <div class="container-fluid">
  <a class="navbar-brand" href="#">
  <h2>MY COMMUNITY</h2>
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Services</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contact.php">Contact</a>
        </li>
        <li class="nav-item">
        <a href="login.php" class="nav-link"><i class="fas fa-lock"></i></a>

        </li>
      </ul>
    </div>
  </div>
</nav>
