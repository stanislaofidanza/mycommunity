<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content">
                    <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="Enter your email">
                        <span class="input-group-btn">
                            <button class="btn" type="submit">Subscribe Now</button>
                        </span>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container" style="margin-top: 50px">
<div class="row">
            <div class="col-lg-3 col-md-6">
                <ul class="list-group-flush">
                    <li class="list-group-item" style="font-weight: bold; font-size:1.3rem"  >CONTACTS</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item" style="font-weight: bold"><i class="fas fa-phone-alt"></i>  9329289898</li>
                    <li class="list-group-item" style="font-weight: bold ;font-size:0.8rem" ><i class="fas fa-envelope"></i> example@mail.com</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6">
            <ul class="list-group-flush">
                    <li class="list-group-item" style="font-weight: bold; font-size:1.3rem"  >Popular</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item"></li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 btn-cont" >
            <ul class="list-group-flush" style="margin-top: 20%">
            <a class="btn btn-outline-dark btn-lg" style="width: 200px; display:block" >Contact Us</a>
            <a class="btn btn-outline-dark btn-lg" style="width: 200px; margin-top:20px">About Us</a>
            </ul>
            </div>
        </div>
</div>
</body>

</html>