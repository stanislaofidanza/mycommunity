<!-- The first include should be config.php -->
<?php require_once('../config.php') ?>
<?php require_once( ROOT_PATH . '/includes/checkauth.php') ?>
<?php require_once( ROOT_PATH . '/includes/public_functions.php') ?>

<!--Ritorna i post dal database---->
<?php $posts = getPublishedPosts(); ?>

<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>

<title>LifeBlog | Admin </title>



<?php require_once(ROOT_PATH . '/admin/navbar_admin.php') ?>
<section class=article>
		<div class="container">
			<div class="row">
				<?php foreach ($posts as $post): ?>
				<div class="col-12 col-sm-8 col-md-6 col-lg-4 mt-4 d-flex">
					<div class="card">
					<img src="<?php echo BASE_URL . '/static/images/' . $post['image']; ?>" class="card-image" alt=""style="max-height: 300px">

						<div class="card-body">
							<h4 class="card-title"><?php echo $post['title'] ?></h4>
							<small class="text-muted cat">
								<i class="far fa-clock text-info"></i> 30 minutes
								<i class="fas fa-users text-info"></i> 4 portions
							</small>
							<p class="card-text"><?php echo $post['description'] ?></p>
							<a type="submit" href="create_post.php?edit-post=<?php echo $post['id'] ?>"
							class="btn btn-outline-success" name="edit_post">Modifica Articolo</a>
						
						</div>
						<div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
							<div class="views"><?php echo $post['created_at']?>
							</div>
							<div class="stats">
								<i class="far fa-eye"></i> <?php echo $post['views']?>
	
							</div>

						</div>
					</div>
				</div>
				<?php endforeach?>

				

	</section>