
<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">

  <div class="container-fluid">
  <a class="navbar-brand" href="#">
  <h2>MY COMMUNITY</h2>
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/index.php">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="/admin/create_post.php">
          <i class="fas fa-plus"></i> Add article


                <span class="sr-only">(current)</span>
              </a>
        </li>


       <?php
      
        if(isset($_POST['logout_btn'])) { 
          $_SESSION['loggedIn'] = 'no';
          header('location: ' . BASE_URL . 'index.php');
              exit(0); 
        } 
      
    ?> 
        <li class="nav-item active">
          <form  action="" method="post">
          <button class="nav-link mynav-link" href="index.php" type="submit" name="logout_btn" >LOGOUT
                <span class="sr-only">(current)</span>
      </button>
          </form>
          
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/informazioni.php" style="color: black">INFORMAZIONI
                <span class="sr-only">(current)</span>
              </a>
        </li>
      </ul>
    </div>
  </div>
</nav>