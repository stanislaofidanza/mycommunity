<?php  include('../config.php'); ?>
<?php  include(ROOT_PATH . '/admin/includes/admin_functions.php'); ?>
<?php 
	// Get all admin users from DB
	$admins = getAdminUsers();
	$roles = ['Admin', 'Author'];				
?>
<?php include(ROOT_PATH . '/includes/head_section.php'); ?>
	<title>Admin | Manage s</title>
</head>
<body>
	<!-- admin navbar -->
	<?php include(ROOT_PATH . '/admin/navbar_admin.php') ?>
		<!-- Left side menu -->
		<!-- Middle form - to create and edit  -->
		
        <div class="container content">

          <div class="action">
          			<h1 class="page-title">Modifica Admin</h1>
          
          			<form method="post" action="<?php echo BASE_URL . 'admin/informazioni.php'; ?>" >
          
          				<!-- validation errors for the form -->
          				<?php include(ROOT_PATH . '/includes/errors.php') ?>
          
          				<!-- if editing user, the id is required to identify that user -->
          				<?php if ($isEditingUser === true): ?>
          					<input type="hidden" name="admin_id" value="<?php echo $admin_id; ?>">
          				<?php endif ?>
          
          				<div class="form-group row">
                    <div class="col-sm-10">
                      <input type="text" name="username" value="<?php echo $username; ?>" placeholder="Username">
                    </div>
                  </div>
          				<div class="form-group row">
                    <div class="col-sm-10">
                      <input type="email" name="email" value="<?php echo $email ?>" placeholder="Email">
                    </div>
                  </div>
          				<div class="form-group row">
                    <div class="col-sm-10">
                      <input type="password" name="password" placeholder="Password">
                    </div>
                  </div>
          				<div class="form-group row">
                    <div class="col-sm-10">
                      <input type="password" name="passwordConfirmation" placeholder="Conferma Password">
                    </div>
                  </div>
          				<select name="role">
          					<option value="" selected disabled>Assign role</option>
          					<?php foreach ($roles as $key => $role): ?>
          						<option value="<?php echo $role; ?>"><?php echo $role; ?></option>
          					<?php endforeach ?>
          				</select>
          
          				<!-- if editing user, display the update button instead of create button -->
          				<?php if ($isEditingUser === true): ?> 
          					<button type="submit" class="btn" name="update_admin">UPDATE</button>
          				<?php else: ?>
          					<button type="submit" class="btn" name="create_admin">Save User</button>
          				<?php endif ?>
          			</form>
          		</div>
        </div>
        		<!-- // Middle form - to create and edit -->
        
        		<!-- Display records from DB-->
              <div class="container mt-5">
                <div class="table-responsive">
                			<!-- Display notification message -->
                			<?php include(ROOT_PATH . '/includes/messages.php') ?>
                
                			<?php if (empty($admins)): ?>
                				<h1>No admins in the database.</h1>
                			<?php else: ?>
                				<table class="table table-editable">
                					<thead>
                						<tr>
                              <th>N</th>
                              						<th>Admin</th>
                              						<th colspan="2">Action</th>
                            </tr>
                					</thead>
                					<tbody>
                					<?php foreach ($admins as $key => $admin): ?>
                						<tr>
                							<td><?php echo $key + 1;?></td>
                							<td>
                								<?php echo $admin['username'];?>, &nbsp;
                								<?php echo $admin['email']; ?>	
                							</td>
                							<td>
                								<a style="color:black"
                                  href="informazioni.php?edit-admin=<?php echo $admin['id'] ?>">
                                  <i class="fas fa-edit"></i>
                								</a>
                							</td>
                							<td>
                								<a class="fa fa-trash btn delete" 
                								    href="informazioni.php?delete-admin=<?php echo $admin['id'] ?>">
                								</a>
                							</td>
                						</tr>
                					<?php endforeach ?>
                					</tbody>
                				</table>
                			<?php endif ?>
                		</div>
              </div>
            
        		<!-- // Display records from DB -->
      
</body>
</html>
